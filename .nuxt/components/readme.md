# Discovered Components

This is an auto-generated list of components discovered by [nuxt/components](https://github.com/nuxt/components).

You can directly use them in pages and other components without the need to import them.

**Tip:** If a component is conditionally rendered with `v-if` and is big, it is better to use `Lazy` or `lazy-` prefix to lazy load.

- `<Back>` | `<back>` (components/Back.vue)
- `<Decoration>` | `<decoration>` (components/Decoration.vue)
- `<Maps>` | `<maps>` (components/Maps.vue)
- `<ModuleTitle>` | `<module-title>` (components/ModuleTitle.vue)
- `<Navbar>` | `<navbar>` (components/Navbar.vue)
- `<PButton>` | `<p-button>` (components/PButton.vue)
- `<PLights>` | `<p-lights>` (components/PLights.vue)
- `<PModal>` | `<p-modal>` (components/PModal.vue)
- `<PNavigation>` | `<p-navigation>` (components/PNavigation.vue)
- `<PNotification>` | `<p-notification>` (components/PNotification.vue)
- `<PProgress>` | `<p-progress>` (components/PProgress.vue)
- `<PSearch>` | `<p-search>` (components/PSearch.vue)
- `<PTitle>` | `<p-title>` (components/PTitle.vue)
- `<PTitleV2>` | `<p-title-v2>` (components/PTitleV2.vue)
- `<Popup>` | `<popup>` (components/Popup.vue)
- `<Profile>` | `<profile>` (components/Profile.vue)
- `<Skeleton>` | `<skeleton>` (components/Skeleton.vue)
- `<IconArchivements>` | `<icon-archivements>` (components/icon/Archivements.vue)
- `<IconAvatarBackground>` | `<icon-avatar-background>` (components/icon/AvatarBackground.vue)
- `<IconBackBtn>` | `<icon-back-btn>` (components/icon/BackBtn.vue)
- `<IconBgAvatar>` | `<icon-bg-avatar>` (components/icon/BgAvatar.vue)
- `<IconBgBack>` | `<icon-bg-back>` (components/icon/BgBack.vue)
- `<IconCluster>` | `<icon-cluster>` (components/icon/Cluster.vue)
- `<IconFaction>` | `<icon-faction>` (components/icon/Faction.vue)
- `<IconFilter>` | `<icon-filter>` (components/icon/Filter.vue)
- `<IconHome>` | `<icon-home>` (components/icon/Home.vue)
- `<IconLocation>` | `<icon-location>` (components/icon/Location.vue)
- `<IconSearch>` | `<icon-search>` (components/icon/Search.vue)
- `<IconSkillcard>` | `<icon-skillcard>` (components/icon/Skillcard.vue)
- `<IconTabSkill>` | `<icon-tab-skill>` (components/icon/TabSkill.vue)
- `<IconTabSkill2>` | `<icon-tab-skill2>` (components/icon/TabSkill2.vue)
- `<FactionReward>` | `<faction-reward>` (components/faction/Reward.vue)
- `<SidebarAvatar>` | `<sidebar-avatar>` (components/sidebar/Avatar.vue)
- `<SidebarCardName>` | `<sidebar-card-name>` (components/sidebar/CardName.vue)
- `<SidebarMain>` | `<sidebar-main>` (components/sidebar/Main.vue)
